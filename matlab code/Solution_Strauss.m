function Solution_s = Solution_Strauss(a, x, y)
% On resoud max( a min x ) = y en connaissant une solution a priori x

n = length(a) ;
Solution_s = x ;

maxi_a = max(a) ;
if( maxi_a < y )
    for i=1:n
        if(maxi_a == a(i)) Solution_s(i) = 1 ; end
%        else Solution_s(i) = rand(1) ;  end
    end
else
    for i=1:n
        if(maxi_a == a(i)) Solution_s(i) = y ; end
%        else Solution_s(i) = rand(1) ; end
    end    
end

