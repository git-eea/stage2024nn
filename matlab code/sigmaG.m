function sigma = sigmaG(x, y, z)
t1 = max( (x-z),0 )/2 ;
t2 = max( (y-z),0 ) ;
sigma = min(t1, t2) ;
