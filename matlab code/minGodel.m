function c = minGodel(A,b)
[n, m] = size(A) ;
if length(b(:)) ~= m return ; end
c = ones(1,n) ;

for i=1:n
    for(j=1:m)
        c(i) = min(c(i), Godel(A(i,j), b(j))) ;
    end
end

