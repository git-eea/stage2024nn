function Solutions = Solution_polonaise(A,b)

[n,m] = size(A) ;
if(length(b)~=n) return ; end

[b, index]= sort(b,'descend') ;

AA = A ;
nn = 0 ;

for i=1:n
    %if( b(i) > 0 )
        nn = nn+1 ;
        AA(nn,:) = A(index(i),:) ; 
    %end
end

A = AA ;

% Calcul de A'

AA = zeros(size(A)) ;

for i=1:n
    for j=1:m
        if ( A(i,j) >= b(i) ) AA(i,j) = A(i,j) ; end
    end
end

T = [] ;
K = [] ;

nq = 0 ;

for j=1:m
    if AA(1,j) > 0
        nq = nq + 1 ;
        K = [j] ;
        T = [b(1)] ;
        Q(nq) =struct('T', {T}, 'K', {K}, 'l', {1}) ;
    end
end

ok = 1 ;
nF = 0 ;
nq = length(Q) ;
V = 1:n ;

while nq > 0
    nqq = 0 ;
    l = Q(1).l ;
    K = Q(1).K ;
    T = Q(1).T ;
    s = l+1;
    VV = [] ;
    V = 1:n ;
    while s <= n 
        av = 0 ;
        for k=1:length(K)
            j = K(k) ;
            av = max(av, min(AA(s,j),T(k))) ;
        end
        if(av<b(s))
            if(~isempty(V==s))
                VV = [VV, s] ;
            end
        end
        s = s+1 ;
    end
    V = VV ;
    if(isempty(V)) 
        nF = nF + 1 ;
        F(nF,:) = zeros(1,m) ;

        for k=1:length(K)
            F(nF,K(k)) = T(k) ;
        end
    else
        d = min(V) ;
        for j=1:m
            if(AA(d,j)>0)
                TT = [T, b(d)] ;
                KK = [K, j] ;
                nqq = nqq+1 ;
                QQ(nqq) = struct('T', {TT}, 'K', {KK}, 'l', {d}) ;                               
            end
        end
    end

    for i=2:nq
        Q(i-1) = Q(i) ;
    end
    nq = nq - 1 ;
    for i=1:nqq
        nq = nq + 1 ;
        Q(nq) = QQ(i) ;
    end

    
    
end

Solutions = F ;


