function c = maxmin(A,b)
[n, m] = size(A) ;
if length(b(:)) ~= m return ; end
c = zeros(1,n) ;

for i=1:n
    for(j=1:m)
        c(i) = max(c(i), min(A(i,j), b(j))) ;
    end
end

