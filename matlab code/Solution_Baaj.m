function Solution_b = Solution_Baaj(A,b)

Delta = Calcul_Delta(A,b) ;
Delta = max(Delta) ;
b_delta = min(1, b + Delta) ;
solution_max = minGodel(A',b_delta) ;
solution_max = maxmin(A,solution_max) ;
solution_max = minGodel(A',solution_max) ;
b_delta = max(0, b - Delta) ;
Solutions_p = Solution_polonaise(A,b_delta) ;
[nombre_de_solutions, n] = size(Solutions_p) ;
% nombre_de_solutions,

Solutions_minimales = solution_max ;
ns = 1 ;
for n=1:nombre_de_solutions
    if(min(solution_max-Solutions_p(n,:))>=0)
        ns = ns + 1 ;
        Solutions_minimales(ns,:) = Solutions_p(n,:) ;
    end
end

delta_min = 1e10 ;
n_min = 1 ;

for n=1:ns
    x = Solutions_minimales(n,:) ;
    b_x = maxmin(A,x) ;
    delta = mean(abs(b(:)-b_x(:))) ;
    if(delta<delta_min)
        delta_min = delta ;
        n_min = n ;
    end
end
Erreur = [delta_min, max(Delta)] ;

Solution_b = Solutions_minimales(n_min,:) ;
