#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

// Function to read configuration file
inline void readConfig(const std::string &filename, int &L, std::vector<int> &M,
                std::vector<double> &X,
                std::vector<std::vector<std::vector<double>>> &W) {
  std::ifstream inputFile(filename);
  std::string line, word;
  std::stringstream ss;
  int num;

  // Read L
  std::getline(inputFile, line);
  ss.str(line);
  ss >> word >> L; // L 2
  ss.clear();

  // Read M
  std::getline(inputFile, line);
  ss.str(line);
  ss >> word; // M
  while (ss >> num) {
    M.push_back(num);
  }
  ss.clear();

  // Read X
  std::getline(inputFile, line);
  ss.str(line);
  ss >> word; // X
  double val;
  while (ss >> val) {
    X.push_back(val);
  }
  ss.clear();

  // Read W
  std::getline(inputFile, line); // W
  std::vector<std::vector<double>> layer;
  while (std::getline(inputFile, line)) {
    if (line == "---") {
      W.push_back(layer);
      layer.clear();
    } else {
      ss.str(line);
      std::vector<double> weights;
      while (ss >> val) {
        weights.push_back(val);
      }
      layer.push_back(weights);
      ss.clear();
    }
  }
  if (!layer.empty()) {
    W.push_back(layer);
  }
}

inline double max(double a, double b) { return (a > b) ? a : b; }
inline double min(double a, double b) { return (a < b) ? a : b; }