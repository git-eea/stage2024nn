#include "reader.hpp"

double maxmin(int n, std::vector<double> x, std::vector<double> w) {
  double y = 0;
  for (int i = 0; i < n; ++i) {
    y = max(y, min(x[i], w[i]));
  }
  return y;
}

double Godel(double a, double b) { return (a <= b) ? 1 : b / a; }

std::vector<double> minGodel(const std::vector<std::vector<double>> &A,
                             const std::vector<double> &b) {
  int n = A.size();
  int m = b.size();
  std::vector<double> c(n, 1.0);

  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      c[i] = std::min(c[i], Godel(A[i][j], b[j]));
    }
  }
  return c;
}

std::vector<double> layerforward(std::vector<double> Xs,
                                 std::vector<std::vector<double>> Ws, int M) {
  std::vector<double> Ys(M);
  int n = Xs.size();
  for (int i = 0; i < M; ++i) {
    Ys[i] = maxmin(n, Xs, Ws[i]);
  }
  return Ys;
}

double FF(int L, std::vector<int> M, std::vector<double> In,
          std::vector<std::vector<std::vector<double>>> W,
          std::vector<std::vector<std::vector<double>>> &interVals) {
  std::vector<double> Xs = In;
  std::vector<double> Ys;
  double Y;
  for (int i = 0; i < L; ++i) {
    Ys = layerforward(Xs, W[i], M[i]); // Layer Forward
    interVals[i] = {Xs, Ys};           // Store intermediate values
    Xs = Ys;                           // updates of Entries
  }

  Y = maxmin(M.size(), Xs, W[L][0]);
  interVals[L] = {Xs, {Y}}; // Store final layer values
  return Y;
}

// Backpropagation function
std::vector<double>
backpropagate(int L, std::vector<int> M,
              std::vector<std::vector<std::vector<double>>> W,
              std::vector<std::vector<std::vector<double>>> interVals) {
  std::vector<double> delta = interVals[L][1];
  std::vector<double> Xs, Ys, grads;
  std::vector<std::vector<double>> Ws;

  for (int i = L - 1; i >= 0; --i) {
    Xs = interVals[i][0];
    Ys = interVals[i][1];
    Ws = W[i];
    grads = minGodel(Ws, delta);
    // Updating delta should use appropriate maxmin calculation per each layer
    for (size_t j = 0; j < Xs.size(); ++j) {
      delta[j] = maxmin(M[i], Xs, grads);
    }
  }

  return delta;
}

////////////////////////////////main////////////////////////////////

int main() {
  int L;
  std::vector<int> M;
  std::vector<double> X;
  std::vector<std::vector<std::vector<double>>> W;

  readConfig("config.txt", L, M, X, W);


  std::vector<std::vector<std::vector<double>>> interVals(
      L + 1); // To store intermediate values


  double out = FF(L, M, X, W, interVals);
  std::cout << "Output: " << out << std::endl;

  std::vector<double> backpropResult = backpropagate(L, M, W, interVals);
  std::cout << "Backpropagation result: ";
  for (double val : backpropResult) {
    std::cout << val << " ";
  }
  std::cout << std::endl;

  return 0;
}